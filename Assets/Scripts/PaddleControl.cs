﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleControl : MonoBehaviour
{
    int ForceMultiplier;
    // Use this for initialization
    void Start()
    {
        Initialise();
    }

    private void Initialise()
    {
        ForceMultiplier = 200;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = transform.position;
        newPos.x = Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,-8f,8f);
        transform.position = newPos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 point = collision.transform.position - transform.position;
        collision.rigidbody.AddForce(new Vector2(point.x, 0)*ForceMultiplier);
    }
}

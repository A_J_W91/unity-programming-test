﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour
{
    Button StartBtn;
    // Use this for initialization
    void Start()
    {
        Initialise();
    }

    private void Initialise()
    {
        StartBtn = GetComponent<Button>();
        StartBtn.onClick.AddListener(Btn_OnClick);
    }

    private void Btn_OnClick()
    {
        SceneManager.LoadScene("Main");
    }
}

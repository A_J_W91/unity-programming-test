﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    List<GameObject> Blocks;
    Rigidbody2D Ball;
    Transform Paddle;
    Vector2 FirstBlockPos;
    Vector3 BallForce;
    // Use this for initialization
    void Start()
    {
        Initialise();
    }

    private void Initialise()
    {
        Blocks = new List<GameObject>();
        Ball = GameObject.Find("Ball").GetComponent<Rigidbody2D>();
        Paddle = GameObject.Find("Paddle").GetComponent<Transform>();
        FirstBlockPos = new Vector2(-7.8f, 4.5f);
        BallForce = Vector3.up * 300;
        StartCoroutine("StartGame");
        SetUpGameBoard();
    }

    private IEnumerator StartGame()
    {
        while (!Input.GetMouseButtonDown(0))
        {
            yield return null;
        }
        Ball.transform.parent = null;
        Ball.AddForce(BallForce);
    }
    // Update is called once per frame
    void Update()
    {
        if (Blocks.Count.Equals(0))
        {
            SceneManager.LoadScene("Start");
        }
        else
        {
            CheckBlocks();
        }
    }

    private void CheckBlocks()
    {
        for (int i = 0; i < Blocks.Count; i++)
        {
            if (Blocks[i].Equals(null))
            {
                Blocks.Remove(Blocks[i]);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SceneManager.LoadScene("Start");
    }

    private void SetUpGameBoard()
    {
        Ball.velocity = Vector2.zero;
        Ball.transform.parent = Paddle;
        Ball.transform.position = Paddle.position + Vector3.up*.4f;
        if (!Blocks.Count.Equals(0))
        {
            foreach (GameObject block in Blocks)
            {
                Destroy(block);
            }
            Blocks.Clear();
        }
        BuildGame();
    }
    private void BuildGame()
    {
        int rows = 5;
        int cols = 13;
        GameObject block = Resources.Load("Prefabs/Block") as GameObject;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                Blocks.Add(Instantiate(block, new Vector3(FirstBlockPos.x + j * block.transform.localScale.x, FirstBlockPos.y - i * block.transform.localScale.y, 0f), new Quaternion()));
            }
        }
    }
}
